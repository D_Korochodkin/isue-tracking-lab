unit User;

interface
   const
        MAXUSER = 25;
        MINUSER = 50;
        MAXTIME = 180;
        MINTIME = 30;
  type Time = record
    minutes : integer;
    seconds : integer;
  end;
  Type
    matrix = array [1..MAXUSER] of Time;
  procedure CreateUser ();
  procedure WriteUser ();
  procedure FindTimeMinMaxMidle ();
  procedure FindExceedingSelectTime ();

implementation

procedure CreateUser ();
Begin

end;
procedure WriteUser ();
Begin

end;
procedure FindTimeMinMaxMidle ();
Begin

end;
procedure FindExceedingSelectTime ();
Begin

end;


end.

